import React, { createContext, useContext, useReducer } from "react";
import PropTypes from "prop-types";
import { postReducer } from "./Reducers";

const Post = createContext();

function Context({ children }) {
  const [state, dispatch] = useReducer(postReducer, {
    posts: [],
  });

  return <Post.Provider value={{ state, dispatch }}>{children}</Post.Provider>;
}

Context.propTypes = {
  children: PropTypes.node,
};

export default Context;

export const PostState = () => {
  return useContext(Post);
};
