export const postReducer = (state, action) => {
  switch (action.type) {
    case "FETCH_POSTS":
      return { ...state, posts: action.payload };

    case "DELETE_POST":
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== action.payload),
      };

    case "ADD_POST":
      return {
        ...state,
        posts: [
          {
            id: new Date().valueOf(),
            title: action.payload.title,
            body: action.payload.body,
          },
          ...state.posts,
        ],
      };

    case "EDIT_POST":
      return {
        ...state,
        posts: [
          ...state.posts.map((post) => {
            if (post.id == action.payload.id) {
              return {
                id: action.payload.id,
                title: action.payload.title,
                body: action.payload.body,
              };
            }
            return post;
          }),
        ],
      };

    default:
      return state;
  }
};
