import React, { useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import instance from "./axios";
import { Header } from "./Components";
import { PostState } from "./Context/Context";
import { AddPost, EditPost, HomePage, SinglePostPage } from "./Pages";

function App() {
  const { dispatch } = PostState();

  useEffect(() => {
    async function getData() {
      try {
        const response = await instance.get("posts");
        if (response.status == 200) {
          dispatch({ type: "FETCH_POSTS", payload: response.data });
        }
      } catch (error) {
        console.log(error);
      }
    }

    getData();
  }, []);

  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" exact element={<HomePage />} />
        <Route path="/add" element={<AddPost />} />
        <Route path="/edit/:postId" element={<EditPost />} />
        <Route path="/posts/:postId" element={<SinglePostPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
