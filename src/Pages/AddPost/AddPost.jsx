import React, { useRef } from "react";
import { useNavigate } from "react-router-dom";
import instance from "../../axios";
import { PostState } from "../../Context/Context";
import "./AddPost.scss";

function AddPost() {
  const title = useRef(null);
  const body = useRef(null);

  let navigate = useNavigate();

  const { dispatch } = PostState();

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      const response = await instance.post("posts", {
        title: title.current.value,
        body: body.current.value,
      });
      if (response.status == 201) {
        navigate("/");
        dispatch({
          type: "ADD_POST",
          payload: { title: response.data.title, body: response.data.body },
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className="addpost">
      <div className="container">
        <form onSubmit={handleSubmit}>
          <label className="formlabel" htmlFor="title">
            Title :
          </label>
          <input
            ref={title}
            type="text"
            placeholder="Enter your title..."
            id="title"
          />
          <label className="formlabel" htmlFor="body">
            Body :
          </label>
          <textarea ref={body} placeholder="Enter your description" id="body" />
          <button type="submit" className="close-btn">
            Add
          </button>
        </form>
      </div>
    </div>
  );
}

export default AddPost;
