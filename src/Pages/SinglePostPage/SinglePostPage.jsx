import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import instance from "../../axios";
import { PostState } from "../../Context/Context";
import "./SinglePostPage.scss";

function SinglePostPage() {
  const param = useParams();
  let navigate = useNavigate();

  const [post, setPost] = useState(null);

  const { state, dispatch } = PostState();

  useEffect(() => {
    async function getData() {
      if (param.postId <= 100) {
        try {
          const response = await instance.get(`posts/${param.postId}`);
          setPost(response.data);
        } catch (error) {
          console.log(error);
        }
      } else {
        const data = state.posts.find((post) => {
          return post.id == param.postId;
        });
        setPost(data);
      }
    }

    getData();
  }, []);

  async function handleDelete() {
    if (confirm(`Are you sure you want to delete : ${post.title} ?`)) {
      try {
        const response = await instance.delete(`posts/${post.id}`);
        if (response.status == 200) {
          dispatch({ type: "DELETE_POST", payload: post.id });
          navigate("/");
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  return (
    <div className="singlepostpage">
      <div className="container">
        {post === null ? (
          <h2 style={{ color: "black" }}>Loading...</h2>
        ) : (
          <>
            <div className="singlepostpage-content">
              <h2>{post.title}</h2>
              <h5>{post.body}</h5>
            </div>

            <div className="singlepostpage-actions">
              <Link className="link" to={`/edit/${post.id}`}>
                <button className="close-btn">Edit</button>
              </Link>
              <button className="close-btn" onClick={handleDelete}>
                Delete
              </button>
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default SinglePostPage;
