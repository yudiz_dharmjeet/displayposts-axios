import React, { useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import instance from "../../axios";
import { PostState } from "../../Context/Context";
import "./EditPost.scss";

function EditPost() {
  const title = useRef(null);
  const body = useRef(null);

  let navigate = useNavigate();
  const param = useParams();

  const { state, dispatch } = PostState();

  let currentPost = state.posts.find((post) => post.id == param.postId);

  async function handleSubmit(e) {
    e.preventDefault();
    if (param.postId <= 100) {
      try {
        const response = await instance.put(`posts/${param.postId}`, {
          title: title.current.value,
          body: body.current.value,
        });
        if (response.status == 200) {
          navigate("/");
          dispatch({
            type: "EDIT_POST",
            payload: {
              id: param.postId,
              title: response.data.title,
              body: response.data.body,
            },
          });
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      navigate("/");
      dispatch({
        type: "EDIT_POST",
        payload: {
          id: param.postId,
          title: title.current.value,
          body: body.current.value,
        },
      });
    }
  }

  return (
    <div className="addpost">
      <div className="container">
        <form onSubmit={handleSubmit}>
          <label className="formlabel" htmlFor="title">
            Title :
          </label>
          <input
            ref={title}
            type="text"
            placeholder="Enter your title..."
            id="title"
            defaultValue={currentPost.title}
          />
          <label className="formlabel" htmlFor="body">
            Body :
          </label>
          <textarea
            ref={body}
            placeholder="Enter your description"
            id="body"
            defaultValue={currentPost.body}
          />
          <button type="submit" className="close-btn">
            Edit
          </button>
        </form>
      </div>
    </div>
  );
}

export default EditPost;
