import React from "react";
import { PostList } from "../../Components";
import "./HomePage.scss";

function HomePage() {
  return (
    <>
      <PostList />
    </>
  );
}

export default HomePage;
