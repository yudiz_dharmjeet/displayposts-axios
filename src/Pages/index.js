import HomePage from "./HomePage/HomePage";
import SinglePostPage from "./SinglePostPage/SinglePostPage";
import AddPost from "./AddPost/AddPost";
import EditPost from "./EditPost/EditPost";

export { HomePage, SinglePostPage, AddPost, EditPost };
