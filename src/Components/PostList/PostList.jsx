import React from "react";
import { PostState } from "../../Context/Context";
import SinglePost from "../SinglePost/SinglePost";
import "./PostList.scss";

function PostList() {
  const { state } = PostState();

  return (
    <div className="postlist">
      <div className="container">
        {state.posts.length == 0 ? (
          <h3 style={{ color: "black" }}>Loading...</h3>
        ) : (
          state.posts.map((post) => {
            return <SinglePost key={post.id} post={post} />;
          })
        )}
      </div>
    </div>
  );
}

export default PostList;
