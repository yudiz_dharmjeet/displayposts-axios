import React, { useState } from "react";
import PropTypes from "prop-types";
import "./SinglePost.scss";
import { PostState } from "../../Context/Context";
import { Link } from "react-router-dom";
import instance from "../../axios";

function SinglePost({ post }) {
  const [actionCardDisplay, setActionCardDisplay] = useState(false);

  const { dispatch } = PostState();

  async function deletePost() {
    if (confirm(`Are you sure you want to delete : ${post.title} ?`)) {
      try {
        const response = await instance.delete(`posts/${post.id}`);
        if (response.status == 200) {
          dispatch({ type: "DELETE_POST", payload: post.id });
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  return (
    <>
      {actionCardDisplay ? (
        <div className="action-singlepost">
          <Link className="link" to={`/posts/${post.id}`}>
            <button className="outline-btn">View</button>
          </Link>
          <Link className="link" to={`/edit/${post.id}`}>
            <button className="outline-btn">Edit</button>
          </Link>
          <button className="outline-btn" onClick={deletePost}>
            Delete
          </button>
          <button
            className="close-btn"
            onClick={() => setActionCardDisplay(false)}
          >
            X
          </button>
        </div>
      ) : (
        <div className="singlepost" onClick={() => setActionCardDisplay(true)}>
          <h3>{post.title}</h3>
          <p>{post.body}</p>
        </div>
      )}
    </>
  );
}

SinglePost.propTypes = {
  post: PropTypes.object,
};

export default SinglePost;
