import React from "react";
import { Link } from "react-router-dom";
import "./Header.scss";

function Header() {
  return (
    <header className="header">
      <div className="container">
        <div className="header__logo">
          <Link className="link" to="/">
            Posty
          </Link>
        </div>
        <nav className="header__nav">
          <Link className="link" to="/add">
            <button className="outline-btn">Add Post</button>
          </Link>
        </nav>
      </div>
    </header>
  );
}

export default Header;
